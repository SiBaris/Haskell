data Exp a = Val a
            | Exp a :+: Exp a
--auch möglich  | Plus (Exp a) (Exp a)
            | Exp a :*: Exp a
            | Exp a :/: Exp a
            | Exp a :-: Exp a
            deriving (Show, Eq)

exp1 :: Exp Int
exp1 = (Val 2 :+: Val 3) :*: Val 4

exp2 :: Exp Int
exp2 = Val 7 :+: (Val 8 :+: Val 5)

eval :: Exp Int -> Int
eval (Val i) = i
eval (e :+: f) = eval e + eval f
eval (e :*: f) = eval e * eval f
eval (e :/: f) = eval e `div` eval f
eval (e :-: f) = eval e - eval f

data ExpOpt a =	ValOpt a
			  | Binary ArithOp (ExpOpt a) (ExpOpt a)
			  deriving Show

data ArithOp = Plus | Times | Div | Minus
              deriving Show

(.+.) :: ExpOpt a -> ExpOpt a -> ExpOpt a
--ethareduktion, wenn eine Funktion links genauso vorkommt wie rechts dann kann man es weglassen
-- (.+.) e f = Binary Plus e f
(.+.) = Binary Plus

(.*.) :: ExpOpt a -> ExpOpt a -> ExpOpt a
(.*.) = Binary Times

(./.) :: ExpOpt a -> ExpOpt a -> ExpOpt a
(./.) = Binary Div

(.-.) :: ExpOpt a -> ExpOpt a -> ExpOpt a
(.-.) = Binary Minus

--schlchter Baum , wegen zu auffändig hinzuschreiben
expOpt1 :: ExpOpt Int
expOpt1 = Binary Times (Binary Plus (ValOpt 2) (ValOpt 3)) (ValOpt 4)

expOpt2 :: ExpOpt Int
expOpt2 = ValOpt 2 .+. ValOpt 3 .*. ValOpt 4


semantics :: ArithOp -> Int -> Int -> Int
semantics Plus = (+)
semantics Times = (*)
semantics Minus = (-)
semantics Div = div

evalOpt :: ExpOpt Int -> Int
evalOpt (ValOpt i) = i
evalOpt (Binary aop e f) = semantics aop (evalOpt e) (evalOpt f)












-- 
data Tree leaf node = Leaf leaf
                    | Node node (Tree leaf node) (Tree leaf node)
                    deriving Show

(.++.) :: ExpTree a -> ExpTree a -> ExpTree a
(.++.) = Node Plus

(.**.) :: ExpTree a -> ExpTree a -> ExpTree a
(.**.) = Node Times

type ExpTree a = Tree a ArithOp

foldTree :: (a -> c) -> (b -> c -> c -> c) -> Tree a b -> c
foldTree lf _ (Leaf x) = lf x
foldTree lf nf (Node n l r) = nf n (foldTree lf nf l) (foldTree lf nf r)


evalTree :: ExpTree Int -> Int
-- n ist arithmetische operation
--evalTree = foldTree id (\ n l r -> semantics n l r)
evalTree = foldTree id semantics

expTree1 :: ExpTree Int
expTree1 = (Leaf 2 .++. Leaf 3) .**. Leaf 4








data RoseTree leaf node = RoseLeaf leaf | RoseNode node [RoseTree leaf node]

data Aop = Add | Inv | Art
           deriving Show


sem :: Aop -> [Int] -> Int
sem Add = sum
sem Inv [x] = negate x
sem Inv _ = error "Inverse is a unary function"
sem Art [x, y ,z] = x * y + z
sem Art _ = error "Art is a ternary function"
