module HelperFunctions where 

import Type
import PrettyPrint

-- tests if a variable is part of a term.
testForVariable :: VarIndex -> Term -> Bool
testForVariable x (Var y)     = x == y
testForVariable x (Comb _ ys) = foldr (\s y -> (testForVariable x s) || y) False ys 

--allNothing :: [Maybe a] -> Bool
allNothing []              = True
allNothing (x:xs)
            | x == Nothing = allNothing xs
            | otherwise    = False

-- checks if a given SLDTree is empty
isEmptyTree :: SLDTree -> Bool
isEmptyTree (SLDTree (Goal []) _) = True
isEmptyTree _ = False

-- selector method for rules
getRightSide:: Rule -> [Term]
getRightSide (_:- rt)= rt

-- selector method for rules
getLeftSide:: Rule -> Term
getLeftSide (rl:-_) = rl

-- checks if two goals are equal
equalGoals :: Goal -> Goal -> Bool
equalGoals (Goal xs) (Goal ys) = and [ if equalTerms (fst x) (snd x) 
                                       then True 
                                       else False | x <- (zip xs ys)]

-- check if two given terms are equal
equalTerms :: Term -> Term -> Bool
equalTerms (Var i) (Var j)      = if i == j then True else False
equalTerms (Comb f ts1) (Comb g ts2) 
                    |f == g && length ts1 == length ts2 
                                = and [equalTerms t1 t2 | (t1, t2) <- (zip ts1 ts2)]
                    | otherwise = False
equalTerms t1 t2 = False



-- checks if Term is a variable and not of type Comb String [Term]
termIsVar :: Term -> Bool
termIsVar (Var i) = True
termIsVar _     = False

-- return true if elem is not in given list
notContains :: VarIndex -> [VarIndex] -> Bool
notContains i xs = and [ if i == x then False else True | x <- xs]

-- First term must be a variable , it will return if first variable is in second term
variableInTerm :: Term -> Term -> Bool
variableInTerm (Var i) (Var j)     = if i == j then True else False
variableInTerm (Var k) (Comb s []) = False
variableInTerm (Var k) (Comb s ts) = and [variableInTerm (Var k) t | t <- ts ]

