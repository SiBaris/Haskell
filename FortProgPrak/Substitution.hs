module Substitution where

import Type
import PrettyPrint

-- Composition of substitutions
-- pretty $ compose substExample1 substExample1
compose :: Subst -> Subst -> Subst
compose (Subst xs) (Subst ys) = Subst (compose' xs ys)

compose' :: [(VarIndex, Term)] -> [(VarIndex, Term)] -> [(VarIndex, Term)]
compose' xs [] = xs  -- second list is empty
compose' [] ys = ys  -- first list is empty
compose' xs (y:ys)                                     
            | elem (snd y) (dom xs) = (compHelp y xs) : (compose' xs ys)          -- 3 : 2 : 1 : []  == [3,2,1]
            | otherwise             = y : (compose' xs ys)                        -- [1,2] ++ [3,4] == [1,2,3,4]
                 where compHelp :: (VarIndex, Term) -> [(VarIndex, Term)] -> (VarIndex, Term)
                       compHelp y (z:zs)  | (snd y) == (Var(fst z)) = (fst y, snd z)   -- (0, Var 1) [(1, Var 3)] ==> (0, Var 3)
                                          | otherwise               = compHelp y zs

-- creates the dom of a substitution
-- unzip :: [(a,b)] -> ([a], [b])
-- [(4, Var 3), (2, Var 5)]  
dom :: [(VarIndex, Term)] -> [Term]
dom xs = map (\x -> (Var x)) (fst (unzip xs))  -- [4,2] ==> [Var 4, Var 2]

-- apply a substitution on a term
-- lookup :: Eq a => a -> [(a,b)] -> Maybe b
-- pretty substExample2 ==> {A -> B}
-- pretty termExample3  ==> p(A,D)
-- pretty $ apply substExample2 termExample3  ==> p(B,D)
apply :: Subst -> Term -> Term
apply (Subst xs) v@(Var   x) = maybe v id (lookup x xs)   -- id > identity function
apply (Subst xs) (Comb f ts) = Comb f (map (apply (Subst xs)) ts)


{-
-- applies a given function to the internal value passed by a JUST but otherwise returns a default value when given Nothing
maybe :: b -> (a -> b) -> Maybe a -> b 
maybe value f (Just x) = f x
maybe value _ Nothing = value
-}