import Data.Char (isAlpha, toLower)
import System.Random (randomRIO)

readNumber :: IO Int
readNumber = do
     putStr "Ihre Zahl: "
     str <- getLine
     case reads str of
      [(n, [])] -> return n
      _ -> putStrLn "Das ist keine Zahl." >> readNumber

randomNumber :: Int -> IO Int
randomNumber n = randomRIO (1, n)

guess :: Int -> IO ()
guess maxVal = do
 n <- randomNumber maxVal
 putStrLn $ "Willkommen zum Raten einer Zahl zwischen 1 und "
               ++ show maxVal
 play 1 n
 where
 play try n = do
 m <- readNumber
 case compare m n of
   LT -> do
     putStrLn "Zu klein."
     play (try + 1) n
   EQ -> putStrLn $ "Hurra, " ++ show try ++ " Versuch(e)."
   GT -> do
     putStrLn "Zu groß."
     play (try + 1) n


-- |State consisting of secret, tried characters
data State = State String [Char]

initState :: String -> State
initState s = State s []

solved :: State -> Bool
solved (State secret guessed) = all (guessed `contains`) secret

contains :: [Char] -> Char -> Bool
contains cs c = not (isAlpha c) || toLower c `elem` map toLower cs

showSecret :: State -> String
showSecret (State secret guessed) = map (\c -> if guessed `contains` c then c else '*') secret

guess' :: Char -> State -> State
guess' c (State sec guessed) = State sec (c:guessed)


hangman :: String -> IO ()
hangman secret = do
  tries <- hangman' (initState secret)
  putStrLn $ "Solved in " ++ show tries ++ " tries."
  where
     hangman' :: State -> IO Int
     hangman' s | solved s  = return 0
                | otherwise = do
                       putStrLn $ "Secret: " ++ showSecret s
                       putStr "Enter a character: "
                       c <- getChar
                       putStrLn ""
                       tries <- hangman' (guess' c s)
                       return (tries + 1)



fac :: Int -> IO Int
fac n 
      | n == 0    = return 1
      | otherwise = do f <- fac (n-1)
                       print (n-1,f)
                       return (n * f)                  
main :: IO ()
main = do putStr "n: "
          str <- getLine
          facn <- fac (read str)
          putStrLn ("Factorial: " ++ show facn)


fibonacci :: Int -> IO Int
fibonacci n 
           | n == 0    = return 0
           | n == 1    = return 1
           | otherwise = do f1 <- fibonacci(n - 1)
                            f2 <- fibonacci (n - 2)
                            print (f1, f2)
                            return (f1 + f2)
main2 :: IO ()
main2 = do putStr "fib: "
           str <- getLine
           fibn <- fibonacci (read str)
           putStrLn ("Fibonacci : " ++ show fibn)



outputConcat = putStr "Moin " >> putStrLn "Hi"