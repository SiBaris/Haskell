module Strategy where

import Type
import Substitution
import HelperFunctions

-- return method of given string
selectStrategy :: String -> (SLDTree -> [Subst])
selectStrategy "bfs" = bfs
selectStrategy "dfs" = dfs

-- ############## breadth-first search ##############
bfs :: SLDTree -> [Subst]
bfs (SLDTree g []) = []
bfs (SLDTree g tuples) = fst $ unzip $ filter (\x -> equalGoals (snd x) g)  
                         (tuplesInBfsOrder $ substOri (Subst []) tuples)

-- Sorting the tuples in BFS Order
tuplesInBfsOrder :: [(Subst, SLDTree)] -> [(Subst, Goal)]
tuplesInBfsOrder [] = []
tuplesInBfsOrder ((s, SLDTree g ys):xs) = (s, g) : tuplesInBfsOrder xs ++ tuplesInBfsOrder ys   

-- substitutions With Original Variable Assignment
substOri :: Subst -> [(Subst, SLDTree)] -> [(Subst, SLDTree)]
substOri old xs = [ (compose old s, SLDTree g (substOri (compose old s) ys)) | (s, SLDTree g ys) <- xs]


-- ############## depth-first search ##############
dfs :: SLDTree -> [Subst]
dfs tree = dfs' (Subst []) tree
 where 
  dfs' :: Subst -> SLDTree -> [Subst]
  dfs' subst (SLDTree g []) = [subst]
  dfs' subst (SLDTree g xs) = concatMap (\(a,b) -> dfs' (compose subst a) b) xs


































{-- dfs
dfs :: SLDTree -> [Subst]
dfs (SLDTree g []) = []
dfs (SLDTree g tuples) = fst $ unzip $ filter (\x -> equalGoals (snd x) g) 
                        (tuplesInDfsOrder $ substOri (Subst []) tuples)
-- Sorting the tuples in DFS Order
tuplesInDfsOrder :: [(Subst, SLDTree)] -> [(Subst, Goal)]
tuplesInDfsOrder [] = []
tuplesInDfsOrder ((s, SLDTree g ys):xs) = (s, g) : tuplesInDfsOrder ys ++ tuplesInDfsOrder xs 
-}