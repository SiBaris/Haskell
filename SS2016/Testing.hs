import Lists
import Test.QuickCheck
import Data.List

idempotence :: [Int] -> Bool
idempotence xs = quicksort (quicksort xs) == quicksort xs

preservation :: [Int] -> Bool
preservation xs = null (xs \\ quicksort xs) 
                  &&
                  null (quicksort xs \\ xs)

smallest_first :: [Int] -> Property
-- head von xs ist nicht definiert daher Vorbedingung festlegen
smallest_first xs = not (null xs) ==> head (quicksort xs) == minimum xs

reference :: [Int] -> Bool
reference xs = quicksort xs == sort xs

referenceClassify :: [Int] -> Property
-- Syntax:  classify prädikat "Bezeichnung" $ Testfall
referenceClassify xs = classify (length xs < 5) "small" $ quicksort xs == sort xs

referenceClassify' :: [Int] -> Property
referenceClassify' xs = classify (length xs < 5) "small" $
                        classify (length xs > 10) "large" $
                        quicksort xs == sort xs

referenceCollect :: [Int] -> Property
-- collect Anzeige $ Testfall
referenceCollect xs = collect (length xs) $ quicksort xs == sort xs
