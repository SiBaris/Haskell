import Prelude hiding (take)

f x = 42

g x = g x

from :: Integer -> [Integer]
from n = n : from (n+1)

take :: Int -> [a] -> [a]
take _ []     = []
take 0 _      = []
take n (x:xs) = x : take (n-1) xs

primes :: [Int]
primes = sieve [2..]

sieve (p:xs) = p : sieve (filter (\x -> x `mod` p /= 0) xs)  

ones = 1: ones

main = let nats = [0..] in do
         print (ones !! 200000000000)
         print (ones !! 200000000000)












