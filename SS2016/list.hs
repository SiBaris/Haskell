import Prelude hiding (takeWhile, length, reverse, (!!), head, tail, last, filter, init,
        null, repeat, cycle, take, drop, dropWhile, (++), splitAt, map,
        zip, unzip, sum, foldl, foldr)

import Data.List  hiding (takeWhile, length, reverse, (!!), head, tail, last, filter, init,
        null, repeat, cycle, take, drop, dropWhile, (++), splitAt, map,
        zip, unzip, sum, foldl, foldr, nub, inits, tails, insert)

xs = [5,6,7,3,2,9,6]

from :: Num a => a -> [a]
from n = n : from (n + 1)

fromTo :: Int -> Int -> [Int]
fromTo from to 
               | from > to = []
               | otherwise = from : fromTo (from + 1) to

length :: [a] -> Int
length [] = 0
length (_:xs) = 1 + length xs

reverse :: [a] -> [a]
reverse [] = []
reverse (x:xs) = reverse xs ++ [x]

reverseAcc :: [a] -> [a]
reverseAcc list = reverseAccAux list []
  where reverseAccAux :: [a] -> [a] -> [a]
        reverseAccAux [] acc = acc
        reverseAccAux (x:xs) acc = reverseAccAux xs (x:acc)

reverse' :: [a] -> [a]
reverse' = foldr (\x ys -> ys ++ [x]) []

reverse'' :: [a] -> [a]
reverse'' = foldl (\acc x -> x : acc) [] 
  where
    revH [] acc = acc
    revH (x:xs) acc = revH xs (x:acc)

-- reverser, reversel :: Eq a => [a] -> [a]
-- reverser = foldr (\x xs -> xs ++ [x]) [] -- quadratisch
-- reversel = foldl (\xs x -> x : xs)       -- linear
-- reversel = foldl (flip (:)) []

(++) :: [a] -> [a] -> [a]
(++) [] xs = xs
(++) xs [] = xs
(++) (x:xs) (y:ys) = x : (++) xs (y:ys) 

-- Get the Nth element out of a list.
(!!) :: [a] -> Int -> a
(!!) (x:_) 0 = x   -- auch möglich:  (x:_) !! 0 = 0
(!!) (x:xs) n = (!!) xs (n - 1) -- auch möglich: (x:xs) !! n = xs !! (n - 1)  


--concat :: [[a]] -> [a]
--concat [] = []
--concat xs:xss = xs ++ concat xss

head :: [a] -> a
head (x:xs) = x

last :: Eq a => [a] -> a
last (x:xs) | xs == []   = x
            | otherwise  = last xs

last' :: Eq a => [a] -> a
last' [x] = x
last' (_:xs) = last' xs

tail :: [a] -> [a]
tail (_:xs) = xs

tails :: [a] -> [[a]]
tails []     = [[]]
tails (x:xs) = (x:xs) : tails xs

insert :: a -> [a] -> [[a]]
insert x [] = [[x]]
insert e (x:xs) =  (e:x:xs) : map (x :) (insert e xs)

perms :: [a] -> [[a]]
perms [] = [[]]
perms (x:xs) = concatMap (insert x) (perms xs)

-- infinite list of a
repeat :: a -> [a]
repeat x = [x] ++ repeat x

repeat' :: a -> [a]
repeat' x = xs 
   where xs = x : repeat' x

-- infinite list (cycle) of a given list
cycle :: [a] -> [a]
cycle xs = xs ++ cycle xs

addtoMiddle :: a -> [a] -> [a]
addtoMiddle e xs = ys ++ [e] ++ zs
   where (ys, zs) = splitAt ((length xs) `div` 2) xs

take :: Int -> [a] -> [a]
take 0 _ = []
take _ [] = []
take n (x:xs) = x : take (n - 1) xs


take' :: Int -> [a] -> [a]
take' n xs | n <= 0      = []
          | otherwise   = case xs of
                           []     -> []
                           (x:xs) -> x : take' (n - 1) xs

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile _ [] = []
takeWhile f (x:xs) | f x = x : takeWhile f xs
                   | otherwise = []

indexOf :: Int -> [Int] -> Maybe Int
indexOf _ []                 = Nothing
indexOf x (y:ys) | x == y    = Just 0
                 | otherwise = case indexOf x ys of
                                 Nothing -> Nothing
                                 Just i  -> Just (i + 1)
-- andere Alternative
indexOfList :: Int -> [Int] -> Maybe Int
indexOfList _ [] = Nothing
indexOfList e (x:xs) = indexofListAux e (x:xs) 0
   where indexofListAux _ [] _ = Nothing
         indexofListAux e (x:xs) i = if i == e then Just i
                                     else indexofListAux e xs (i + 1)

drop :: Int -> [a] -> [a]
drop 0 xs = xs
drop n (x:xs) = drop (n - 1) xs

dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile _ [] = []
dropWhile f xs@(y:ys) -- @ ist der "Read As" Operator
                   | f y = dropWhile f ys
                   | otherwise = xs

splitAt :: Int -> [a] -> ([a], [a])
splitAt _ [] = ([], [])
splitAt 0 xs = ([], xs)
splitAt n xs = (ys, zs)
   where ys = take n xs
         zs = take ((length xs) - n) xs

-- test weather a list is empty
null :: [a] -> Bool
null [] = True
null (_:_) = False

init :: [a] -> [a]
init [x] = []
init (x:xs) = x : init xs

inits :: [a] -> [[a]]
inits [] = [[]]
inits (x : xs) = [] : map (x :) (inits xs)

filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter p (x:xs)  | p x       = x : filter p xs
                 | otherwise = filter p xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' f xs = [x | x <- xs, f x]

filter'' :: (a -> Bool) -> [a] -> [a]
filter'' p = foldr (\x ys -> if p x then x:ys else ys) []

filter2 :: (a -> Bool) -> [a] -> [a] -> [a]
filter2 p [] [] = []
filter2 p (t:ts) [] 
                        | p t       = t : filter2 p [] ts
                        | otherwise = filter2 p [] ts
filter2 p [] (z:zs) 
                        | p z       = z : filter2 p [] zs
                        | otherwise = filter2 p [] zs
filter2 p z@(x:xs) t@(y:ys)
                        | p x       = x : filter2 p xs t
                        | p y       = y : filter2 p z ys
                        | otherwise = filter2 p xs ys

-- check if all elements in list passes the condition
andMap :: (a -> Bool) -> [a] -> Bool
andMap f [] = True
andMap f (x:xs) = f x && andMap f xs

-- check if one or more element in list passes the condition
orMap :: (a -> Bool) -> [a] -> Bool
orMap f [] = False
orMap f (x:xs) = f x || orMap f xs

map :: (a -> b) -> [a] -> [b]
map f [] = []
map f (x:xs) = f x : map f xs

map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\x -> (f x:)) []

map'' :: (a -> b) -> [a] -> [b]
map'' f = foldl (\xs x -> xs ++ [f x]) []

incList :: [Int] -> [Int]
incList [] = []
incList (x:xs) = (x + 1) : incList xs

incList' :: Num a => [a] -> [a]
incList' xs = map (+1) xs

sum :: [Integer] -> Integer
sum [] = 0
sum (x:xs) = x + sum xs

sum' :: [Integer] -> Integer
sum' = foldr (+) 0

zip :: [a] -> [b] -> [(a,b)]
zip _ [] = []
zip [] _ = []
zip (x:xs) (y:ys) = (x, y) : zip xs ys

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys
zipWith' _ _ _ = []

unzip :: [(a, b)] -> ([a] , [b])
unzip [] = ([], [])
unzip ((x,y):xys) =
                    let (xs, ys) = unzip xys
                    in (x:xs, y:ys)

unzip' :: [(a, b)] -> ([a] , [b])
unzip' [] = ([], [])
unzip'((x, y):xys) = (x:xs, y:ys)
   where 
   (xs, ys) = unzip' xys

unzipr, unzipl :: [(a, b)] -> ([a], [b])
unzipr = foldr (\(a,b) (as,bs) -> (a : as, b : bs)) ([],[])
unzipl = foldl (\(as,bs) (a,b) -> (as ++ [a], bs ++ [b])) ([],[])

-- check if a given Element is in the List
isElem :: Eq a => a -> [a] -> Bool
isElem e [] = False
isElem e (x:xs)
                | e == x    = True
                | otherwise = isElem e xs

isElem' :: Eq a => a -> [a] -> Bool
isElem' x [] = False
isElem' x (y:ys) = x == y || isElem x ys

--total count of an element in a given list
countElems :: Eq a => a -> [a] -> Int
countElems e [] = 0
countElems e (x:xs)
                    | e == x    = 1 + countElems e xs 
                    | otherwise = countElems e xs


foldr :: (a -> b -> b) -> b -> [a] -> b 
foldr _ e []     = e
foldr f e (x:xs) = f x (foldr f e xs)

foldl :: (a -> b -> a) -> a -> [b] -> a
foldl _ e []     = e
foldl f e (x:xs) = foldl f (f e x) xs

while :: (a -> Bool) -> (a -> a) -> a -> a
while p f x | p x       = while p f (f x)
            | otherwise = x

-- deletes double Elements in a list
nub :: Eq a => [a] -> [a]
nub [] = []
nub (x:xs) = x : nub (filter (/= x) xs)

nubr, nubl :: Eq a => [a] -> [a]
nubr ys = foldr (\x xs -> x : filter (x/=) xs) [] ys
nubl ys = foldl (\xs x -> filter (x/=) xs ++ [x]) [] ys

quicksort :: Ord a => [a] -> [a] 
quicksort [] = [] 
quicksort (x:xs) = quicksort (filter (<= x) xs) ++ [x] ++ quicksort (filter (> x) xs)

quicksort2 :: Ord a => [a] -> [a] -> [a]
quicksort2 [] [] = []
quicksort2 [] xs = quicksort xs
quicksort2 ys [] = quicksort ys
quicksort2 xs ys = quicksort ((quicksort xs) ++ (quicksort ys))

mergesort :: Ord a => [a] -> [a]
mergesort [] = []
mergesort xs = sort (map (:[]) xs)
   where
      sort [] = []
      sort [xs] = xs
      sort xss = sort (mergePairs xss)

mergePairs :: Ord a => [[a]] -> [[a]]
mergePairs [] = []
mergePairs [x] = [x]
mergePairs (xs0:xs1:[]) = [merge xs0 xs1]
mergePairs (xs0:xs1:xss) = merge xs0 xs1 : mergePairs xss

-- Besser :
mergePairs' :: Ord a => [[a]] -> [[a]]
mergePairs' (xs0:xs1:xss) = merge xs0 xs1 : mergePairs' xss
mergePairs' xss = xss

merge :: Ord a => [a] -> [a] -> [a]
merge [] xs = xs
merge ys [] = ys
merge xs@(x:xs1) ys@(y:ys1)
                            | x <= y    = x : merge xs1 ys
                            | otherwise = y : merge xs ys1

findMax :: Ord a => [a] -> a
findMax [x] = x
findMax (x0:x1:xs)
                   | x0 >= x1  = findMax (x0:xs)
                   | otherwise = findMax (x1:xs)


findMaximum :: Ord a => [a] -> a
findMaximum (x:xs)
                  | filter (>x) xs == [] = x
                  | otherwise            = findMaximum xs

ys = [2,6,4]

id = foldr (:) []
prod = foldl (*) 1
minOneR = foldr (-) 1
minOneL = foldl (-) 1

fst :: (a, b) -> a
fst (x, _) = x

snd :: (a, b) -> b
snd (_, y) = y

