data Tree = Leaf Int | Node Int Tree Tree | Empty
        deriving(Eq, Show)

t1 = (Node 4 (Node 1 (Leaf 2) (Leaf 3)) (Node 6 (Leaf 5) Empty))

sumTree :: Tree -> Int
sumTree Empty = 0
sumTree (Leaf x) = x
sumTree (Node x leftTree rightTree) = x + sumTree leftTree + sumTree rightTree