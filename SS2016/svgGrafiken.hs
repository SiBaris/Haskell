-- Verbundtyp
data Point = P Double Double

xCoord:: Point -> Double
xCoord (P x _) = x

yCoord:: Point -> Double
yCoord (P _ y) = y

--Komplexer Typ
data ObjectWithOutStlye = LinieWithOutStlye Point Point 
              | RectangleWithOutStlye Point Point 
              | CircleWithOutStlye Point Double

-- with Style
data Object = Linie Point Point Style
              | Rectangle Point Point Stlye
              | Circle Point Double Stlye

-- Auflistungstyp
data Color = Black   -- Konstruktoren müssen immer groß geschrieben werden.
            | Green 
            | Blue 
            | Red
   deriving Show -- bringt eine Funktion show:: Color -> String" mit, die leider mit Großbuchstaben anfängt

data Style = S Color 

stylteToAttr :: Style -> String
stylteToAttr (S c) = "stroke : " ++ colorToString c ++ "; fill: " ++ colorToString c
   where 
       colorToString Black = "black"
       colorToString Red = "red"
       colorToString Blue = "blue"
       colorToString Green = "green"
       
