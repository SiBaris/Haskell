module Vorlesung.Test where

square x = x * x

min x y = if x <= y then x else y

--Factorial
fac n = if n == 0 then 1 else n * fac (n - 1)

--Vermeidung von sich wiederholenden Ausdrücken mit let
ohneLet x y = y * (1 - y) + (1 + x * y) * (1 - y) + x * y

mitLet x y =
            let a = 1 - y
                b = x * y
            in y * a + (1 + b) * a + b